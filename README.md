# Xentral Extended Woocommerce Shopimporter #


### Idea ###

This code extends the usual xentral shopimporter through different additions, regarding image handling, product attribute handling.
* Current Version: 0.0.2

### Requirements ###

* The needed Wordpress and Woocommerce configuration to run xentral
* A Wordpress User with read and write capabilitys, best case with the role 'administrator'
* The Plugin 'miniOrange API Authentication' should be installed with activated basic authentication

### How do I get set up? ###

* Place the php file in the /www/pages/ Folder of your xentral installation.
* add the custom shopimporter through the database, refere to xentral for further advice

### Who do I talk to? ###

* For Questions, suggestions and bug please contact Maximilian Krebs - m.krebs@sugarpool.de